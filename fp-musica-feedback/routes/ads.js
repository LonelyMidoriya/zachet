const express = require('express');
const router = express.Router();
const reqlib = require('app-root-path').require;
const logger = reqlib('logger');

const jsf = require('json-schema-faker');
const util = require('util')
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

var recentDays = 5;

var schema = {
  "type": "array",
  "minItems": 3,
  "maxItems": 10,
  "items": {
	  "type": "object",
	  "properties": {
	    "productName": {
	      "type": "string",
	      "faker": "commerce.productName"
	    },
	    "companyName": {
	      "type": "string",
	      "faker": "company.companyName"
	    },
	    "price" : {
	      "type": "string",
	      "faker": "commerce.price"
	    },
	  "img" : {
	      "type": "string",
	      "faker": "random.image"
	    }
	  },
	  "required": [
	    "productName", 
	    "companyName", 
	    "price", 
	    "img"
	   ]
	  }
};

/* GET home page. */
router.get('/', (req, res) => {

  jsf.resolve(schema).then(sample => {
  	   logger.debug(util.inspect(sample, 
  	   	{showHidden: false, depth: null}));
	   
	   res.render('ads', 
	  	{  opinions:  sample });
  });

  
});

module.exports = router;
